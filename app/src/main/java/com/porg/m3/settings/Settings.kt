/*
 *     Settings.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.m3.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.porg.gugal.R

/**
 * The inner padding of a setting.
 */
val SettingsPadding = 20.dp

/**
 * The padding between the title and body of a setting.
 */
val SettingsDividerPadding = 5.dp

/**
 * A setting.
 *
 * @param title the ID of the string containing the setting's name.
 * @param body the ID of the string containing the setting's description.
 * @param onClick the action to perform when the setting is tapped.
 *
 * @sample com.porg.m3.Samples.RegularSettingSample
 */
@Composable
fun RegularSetting(title: Int, body: Int, onClick: (() -> Unit)) {
    RegularSetting(
        stringResource(title),
        stringResource(body),
        onClick
    )
}

/**
 * A setting.
 *
 * @param title the setting's name.
 * @param body the setting's description.
 * @param onClick the action to perform when the setting is tapped.
 *
 * @sample com.porg.m3.Samples.RegularSettingSample
 */
@Composable
fun RegularSetting(title: String, body: String, onClick: (() -> Unit)) {
    Column(
        modifier = Modifier
            .clickable { onClick() }
            .fillMaxWidth(),
    ) {
        Text(
            text = title,
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding, top = SettingsPadding, bottom = SettingsDividerPadding),
            style = MaterialTheme.typography.titleLarge
        )
        Text(
            text = body,
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding, bottom = SettingsPadding),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

/**
 * A setting that's still in the preview stage.
 *
 * This adds a [PreviewChip] next to the setting's name.
 *
 * @param title the setting's name.
 * @param body the setting's description.
 * @param onClick the action to perform when the setting is tapped.
 *
 * @sample com.porg.m3.Samples.RegularPreviewSettingSample
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegularPreviewSetting(title: String, body: String, onClick: (() -> Unit)) {
    Column(
        modifier = Modifier
            .clickable { onClick() }
            .fillMaxWidth(),
    ) {
        Row(
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding, top = SettingsPadding, bottom = SettingsDividerPadding),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = title,
                style = MaterialTheme.typography.titleLarge
            )
            PreviewChip()
        }
        Text(
            text = body,
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding, bottom = SettingsPadding),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

/**
 * A simple indicator that says "preview".
 */
@Composable
fun PreviewChip() {
    OutlinedCard (
        modifier = Modifier.padding(start = 8.dp),
        colors = CardDefaults.outlinedCardColors(
            contentColor = MaterialTheme.colorScheme.secondary
        )
    ) {
        Text(
            text = stringResource(R.string.preview),
            modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}


@Composable
fun RadioSetting(title: Int, body: Int, onClick: (() -> Unit), selected: Boolean, modifier: Modifier = Modifier) {
    RadioSetting(
        stringResource(title),
        stringResource(body),
        onClick,
        selected,
        modifier
    )
}

/**
 * A setting with a radio button.
 *
 * @param title the setting's name.
 * @param body the setting's description.
 * @param onClick the action to perform when the setting is tapped.
 */
@Composable
fun RadioSetting(title: String, body: String, onClick: (() -> Unit), selected: Boolean, modifier: Modifier = Modifier) {
    Row(modifier = Modifier
        .clickable { onClick() }
        .then(modifier),
        verticalAlignment = Alignment.CenterVertically) {
        RadioButton(selected = selected,
            onClick = onClick,
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding)
        )
        Column {
            Text(
                text = title,
                modifier = Modifier.padding(end = SettingsPadding, top = SettingsPadding, bottom = SettingsDividerPadding),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = body,
                modifier = Modifier.padding(end = SettingsPadding, bottom = SettingsPadding),
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

/**
 * A setting with a checkbox.
 *
 * @param title the setting's name.
 * @param body the setting's description.
 * @param onCheckedChange the action to perform when the checkbox is toggled.
 */
@Composable
fun CheckboxSetting(title: Int, body: Int, onCheckedChange: (Boolean) -> Unit, checked: Boolean, modifier: Modifier = Modifier.fillMaxWidth()) {
    CheckboxSetting(
        stringResource(title),
        stringResource(body),
        onCheckedChange,
        checked,
        modifier
    )
}

@Composable
fun CheckboxSetting(title: String, body: String, onCheckedChange: (Boolean) -> Unit,
                    checkedProvider: () -> Boolean, modifier: Modifier = Modifier.fillMaxWidth()) {
    CheckboxSetting(title, body, onCheckedChange, checkedProvider(), modifier)
}

/**
 * The title of a settings section - small, bold text.
 * (Note: not to be confused with the title of a settings _page_ - for that use a [LargeTopAppBar])
 *
 * @param text the text to show.
 */
@Composable
fun SettingsTitle(text: String) {
    Text(
        text,
        modifier = Modifier.padding(SettingsPadding),
        fontWeight = FontWeight.Bold,
        color = MaterialTheme.colorScheme.primary
    )
}

@Composable
fun CheckboxSetting(title: String, body: String, onCheckedChange: (Boolean) -> Unit, checked: Boolean, modifier: Modifier = Modifier.fillMaxWidth()) {
    Row(modifier = Modifier
        .clickable { onCheckedChange(!checked) }
        .then(modifier),
        verticalAlignment = Alignment.CenterVertically) {
        Checkbox(checked = checked,
            onCheckedChange = onCheckedChange,
            modifier = Modifier.padding(start = SettingsPadding, end = SettingsPadding)
        )
        Column {
            Text(
                text = title,
                modifier = Modifier.padding(end = SettingsPadding, top = SettingsPadding, bottom = SettingsDividerPadding),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = body,
                modifier = Modifier.padding(end = SettingsPadding, bottom = SettingsPadding),
                style = MaterialTheme.typography.bodyMedium
            )
        }
    }
}

@Composable
fun ToggleSetting(title: Int, body: Int, onCheckedChange: ((Boolean) -> Unit), selected: Boolean) {
    ToggleSetting(
        stringResource(title),
        stringResource(body),
        onCheckedChange,
        selected
    )
}

@Composable
fun ToggleSetting(title: String, body: String, onCheckedChange: ((Boolean) -> Unit), selected: Boolean) {
    Row(
        modifier = Modifier
            .clickable { onCheckedChange(!selected) }
            .fillMaxWidth()
            .padding(start = SettingsPadding, end = SettingsPadding),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement  =  Arrangement.SpaceBetween
    ) {
        Column () {
            Text(
                text = title,
                modifier = Modifier.padding(top = SettingsPadding, bottom = SettingsDividerPadding),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = body,
                modifier = Modifier.padding(bottom = SettingsPadding),
                style = MaterialTheme.typography.bodyMedium
            )
        }
        Switch(
            checked = selected,
            onCheckedChange = onCheckedChange,
        )
    }
}

@Preview
@Composable
private fun RegularSettingPreview() {
    Surface() {
        RegularSetting(R.string.setting_about_title, R.string.setting_about_desc) {}
    }
}


@Preview
@Composable
private fun RegularPreviewSettingPreview() {
    Surface() {
        RegularPreviewSetting(
            "Robin",
            "Your search companion powered by AI"
        ) {
            // show the settings for this feature
        }
    }
}

@Preview
@Composable
private fun SettingsTitlePreview() {
    Surface() {
        SettingsTitle(text = "Cool stuff")
    }
}