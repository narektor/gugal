/*
 *     MainSwitch.kt
 *     M3
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.m3

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

private val settingslib_switchbar_margin = 16.dp
private val settingslib_switchbar_padding_left = 24.dp
private val settingslib_switchbar_padding_right = 16.dp
private val settingslib_min_switch_width = 52.dp
private val settingslib_min_switch_bar_height = 72.dp
private val settingslib_switch_bar_radius = 28.dp
private val settingslib_switch_title_margin = 16.dp

/**
 * A switch and a title on a large card.
 *
 * Used in Android's settings app to toggle a setting with many submenus.
 *
 * @param title the text to show next to the switch.
 * @param checked is the switch enabled?
 * @param onCheckedChange the action to perform when the switch is toggled.
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainSwitch(title: String, checked: Boolean, onCheckedChange: ((Boolean) -> Unit)?) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(settingslib_switchbar_margin),
        shape = RoundedCornerShape(settingslib_switch_bar_radius),
        onClick = {
            if (onCheckedChange != null) {
                onCheckedChange(!checked)
            }
        },
        colors = CardDefaults.cardColors(
            containerColor =
            if (checked) MaterialTheme.colorScheme.primaryContainer
            else MaterialTheme.colorScheme.secondaryContainer
        )
    ) {
        Row(
            modifier = Modifier
                .padding(
                    start = settingslib_switchbar_padding_left,
                    end = settingslib_switchbar_padding_right
                )
                .defaultMinSize(minHeight = settingslib_min_switch_bar_height),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                title,
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(
                    start = 0.dp,
                    top = settingslib_switch_title_margin,
                    end = settingslib_switch_title_margin,
                    bottom = settingslib_switch_title_margin
                )
            )
            Spacer(Modifier.weight(1f))
            Switch(
                checked = checked,
                onCheckedChange = { chk ->
                    // do stuff with chk
                    if (onCheckedChange != null) {
                        onCheckedChange(chk)
                    }
                }
            )
        }
    }
}

@Preview
@Composable
fun MainSwitchPreview() {
    Surface() {
        MainSwitch(title = "Enable Wi-Fi", checked = true) {

        }
    }
}