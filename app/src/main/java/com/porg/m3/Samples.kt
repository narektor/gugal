/*
 *     Samples.kt
 *     Gugal
 *     Copyright (c) 2024 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.m3

import androidx.compose.runtime.Composable
import com.porg.gugal.R
import com.porg.m3.settings.RegularPreviewSetting
import com.porg.m3.settings.RegularSetting

class Samples {
    @Composable
    fun RegularSettingSample() {
        RegularSetting(
            R.string.setting_about_title,
            R.string.setting_about_desc
        ) {
            // show the about screen here
        }
    }
    @Composable
    fun RegularPreviewSettingSample() {
        RegularPreviewSetting(
            "Robin",
            "Your search companion powered by AI"
        ) {
            // show the settings for this feature
        }
    }
}