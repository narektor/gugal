/*
 *     AboutActivity.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import com.porg.gugal.BuildConfig
import com.porg.gugal.ChangelogActivity
import com.porg.gugal.DONATE_URL
import com.porg.gugal.ISSUE_URL
import com.porg.gugal.PREVIEW_ISSUE_URL
import com.porg.gugal.R
import com.porg.gugal.REPO_URL
import com.porg.gugal.TRANSLATE_URL
import com.porg.gugal.open
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.Tip
import com.porg.m3.settings.RegularSetting
import com.porg.m3.settings.SettingsDividerPadding
import com.porg.m3.settings.SettingsPadding
import java.util.Locale

class AboutActivity : ComponentActivity() {

    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val context = this
        val currentLocale: Locale = resources.configuration.locale
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            GugalTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.verticalScroll(rememberScrollState()).statusBarsPadding()
                    ) {
                        IconButton(
                            onClick = { finish() },
                            modifier = Modifier.padding(
                                start = 16.dp,
                                top = 16.dp,
                                bottom = 0.dp,
                                end = 16.dp
                            )
                        ) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Go back",
                            )
                        }
                        Column(modifier = Modifier.fillMaxSize()) {
                            // Gugal logo
                            Card(
                                modifier = Modifier
                                    .padding(all = 24.dp)
                                    .size(128.dp)
                                    .align(alignment = Alignment.CenterHorizontally),
                                shape = CircleShape,
                                backgroundColor = colorResource(R.color.icon_bg)
                            ) {
                                Image(
                                    painterResource(R.drawable.ic_launcher_foreground),
                                    contentDescription = getString(R.string.desc_logo),
                                    contentScale = ContentScale.Crop,
                                    modifier = Modifier.fillMaxSize()
                                )
                            }
                            Text(
                                text = "Gugal " + getVersion(),
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Center,
                                style = MaterialTheme.typography.titleLarge
                            )
                        }
                        if (BuildConfig.VERSION_NAME.contains(".p")) {
                            Tip(
                                text = getString(R.string.tip_preview),
                                modifier = Modifier.padding(
                                    top = 24.dp,
                                    start = 16.dp,
                                    end = 16.dp
                                ), icon = R.drawable.ic_info,
                                onClick = { open(context, PREVIEW_ISSUE_URL) }
                            )
                        }
                        RegularSetting(R.string.setting_donate_title,
                            R.string.setting_donate_desc,
                            onClick = { open(context, DONATE_URL) }
                        )
                        RegularSetting(
                            R.string.setting_issue_title,
                            R.string.setting_issue_desc,
                            onClick = { open(context, ISSUE_URL) }
                        )
                        RegularSetting(
                            R.string.setting_gitlab_title,
                            R.string.setting_gitlab_desc,
                            onClick = { open(context, REPO_URL) }
                        )
                        RegularSetting(
                            R.string.setting_changelog_title,
                            R.string.setting_changelog_desc,
                            onClick = {
                                val intent = Intent(context, ChangelogActivity::class.java)
                                ContextCompat.startActivity(context, intent, null)
                            }
                        )
                        RegularSetting(
                            R.string.setting_translate_title,
                            R.string.setting_translate_desc,
                            onClick = { open(context, TRANSLATE_URL) }
                        )
                        Divider(
                            modifier = Modifier
                                .fillMaxWidth()
                                .width(1.dp)
                                .padding(start = 16.dp, end = 16.dp)
                        )
                        Text(
                            text = getString(R.string.about_contributors_start),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 24.dp),
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.bodyMedium
                        )

                        Contributor(
                            context,
                            "narektor",
                            if (currentLocale.language == "ru") getString(R.string.role_developer_trans)
                            else getString(R.string.role_developer),
                            R.drawable.cont_narektor,
                            "https://gitlab.com/narektor"
                        )
                        Contributor(
                            context,
                            "iSteven",
                            getString(R.string.role_designer),
                            R.drawable.cont_steven,
                            "https://dribbble.com/onesteven"
                        )
                        ContributorForLocale(currentLocale, context)
                    }
                }
            }
        }
    }

    @Composable
    fun ContributorForLocale(locale: Locale, context: Context) {
        // Find the language code at https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry,
        // it's usually written after "Subtag:".
        // Don't add emails as credit URLs, as was the case with Gugal 0.8.1 and earlier; only
        // add GitLab account links or personal websites and don't pass a URL otherwise.
        when (locale.language) {
            // Languages with >=75% translated
            "pl" -> {
                Contributor(
                    context,
                    "Eryk Michalak",
                    getString(R.string.role_translator),
                    R.drawable.cont_eryk
                )
            }
            "it" -> {
                Contributor(
                    context,
                    "Andrea",
                    getString(R.string.role_translator),
                    R.drawable.cont_andrea
                )
            }
            "uk" -> {
                Contributor(
                    context,
                    "Kefir",
                    getString(R.string.role_translator),
                    R.drawable.cont_kefir,
                    "https://gitlab.com/denqwerta"
                )
                Contributor(
                    context,
                    "Dan",
                    getString(R.string.role_translator),
                    R.drawable.cont_dan
                )
            }
            "ko" -> {
                Contributor(
                    context,
                    "Junghee",
                    getString(R.string.role_translator),
                    R.drawable.cont_junghee,
                    "https://gitlab.com/Junghee_Lee"
                )
            }
            "tr" -> {
                Contributor(
                    context,
                    "metezd",
                    getString(R.string.role_translator),
                    R.drawable.cont_metezd
                )
                Contributor(
                    context,
                    "Oğuz Ersen",
                    getString(R.string.role_translator),
                    R.drawable.cont_ersen,
                    "https://ersen.moe/"
                )
            }
            "de" -> {
                Contributor(
                    context,
                    "Martin",
                    getString(R.string.role_translator),
                    R.drawable.cont_martin
                )
                Contributor(
                    context,
                    "Frau Gülle",
                    getString(R.string.role_translator),
                    R.drawable.cont_frauguelle
                )
            }
            "in" -> {
                Contributor(
                    context,
                    "Linerly",
                    getString(R.string.role_translator),
                    R.drawable.cont_linerly,
                    "https://linerly.tk"
                )
                Contributor(
                    context,
                    "Reza",
                    getString(R.string.role_translator),
                    R.drawable.cont_reza,
                    "https://gitlab.com/rezaalmanda"
                )
            }
            // Languages with >=15% translated
            "nb" -> {
                Contributor(
                    context,
                    "Allan Nordhøy",
                    getString(R.string.role_translator),
                    R.drawable.cont_kingu,
                    "https://gitlab.com/kingu"
                )
            }
            "fr" -> {
                Contributor(
                    context,
                    "J. Lavoie",
                    getString(R.string.role_translator),
                    R.drawable.cont_lavoie
                )
            }
            "ta" -> {
                Contributor(
                    context,
                    "தமிழ்நேரம்",
                    getString(R.string.role_translator),
                    R.drawable.cont_anish,
                    "https://gitlab.com/anishprabu.t"
                )
            }
            // Please don't add contributors for languages with less than 15% translated.
        }

        // Special case for Simplified Chinese (zh-Hans)
        if (locale.language == "zh" && locale.script == "Hans") {
            Contributor(
                context,
                "Hugel",
                getString(R.string.role_translator),
                R.drawable.cont_hugel
            )
        }
    }

    // A component displaying information about a contributor.
    @Composable
    fun Contributor(context: Context, name: String, role: String, picture: Int, url: String = "") {
        // Only make the entry clickable if there is a URL
        val maybeClickable: Modifier =
            if (url != "")
                Modifier.clickable { open(context, url) }
            else Modifier

        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(SettingsPadding)
            .then(maybeClickable),
            verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(picture),
                contentDescription = String.format(stringResource(R.string.about_cont_picture), name),
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape)
            )
            Column(modifier = Modifier.padding(start = 15.dp)) {
                Text(
                    text = name,
                    modifier = Modifier.padding(bottom = SettingsDividerPadding),
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = role,
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }

    private fun getVersion(): String {
        val split = BuildConfig.VERSION_NAME.split(".p")
        if (split.size == 2) {
            return "${split[0]} preview ${split[1]}"
        }
        return BuildConfig.VERSION_NAME
    }
}