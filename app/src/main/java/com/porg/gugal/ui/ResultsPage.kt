/*
 *     ResultsPage.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.app.Activity
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.CustomAccessibilityAction
import androidx.compose.ui.semantics.clearAndSetSemantics
import androidx.compose.ui.semantics.customActions
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.gson.Gson
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.fullSearchHistory
import com.porg.gugal.Global.Companion.maxSearchHistoryLength
import com.porg.gugal.Global.Companion.serpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.Global.Companion.visibleSearchHistory
import com.porg.gugal.R
import com.porg.gugal.devopt.DevOptMainActivity
import com.porg.gugal.providers.responses.ErrorResponse
import com.porg.gugal.providers.responses.InvalidCredentialResponse
import com.porg.gugal.setup.SetupConfigureSerpActivity
import com.porg.gugal.ui.Components.Companion.GugalLogo
import com.porg.gugal.ui.Components.Companion.Results
import com.porg.gugal.ui.theme.shapeScheme
import com.porg.gugal.viewmodel.ResultViewModel

val cardElevation = 15.dp
var lastSearchQuery = ""

@OptIn(ExperimentalMaterial3Api::class)
@ExperimentalAnimationApi
@Composable
fun ResultPage(
    context: Activity,
    searchQuery: String,
    resultModel: ResultViewModel = viewModel(),
    windowSizeClass: WindowSizeClass
) {
    val composeDone = remember { mutableStateOf(false) }
    val resultState by resultModel.uiState.collectAsState()
    resultModel.createQueue(context)

    resultModel.updateSizeClass(windowSizeClass.widthSizeClass)

    Column {
        val error = remember { mutableStateOf<ErrorResponse?>(null) }
        val errorResponse by error

        // top card
        AnimatedVisibility (resultState.showLogo) {
            Box(
                modifier = Modifier.fillMaxWidth()
            ) {
                GugalLogo(modifier = Modifier.align(alignment = Alignment.Center))
            }
        }

        // Automatically handle some error responses
        if (errorResponse != null) {
            when (errorResponse) {
                // If an InvalidCredentialResponse is received, open the setup page
                is InvalidCredentialResponse -> {
                    val intent = Intent(context, SetupConfigureSerpActivity::class.java)
                    intent.putExtra("launchedFromSettings", true)
                    startActivity(context, intent, null)
                    error.value = null
                }
            }
        }

        val tfSidePadding = if (resultState.showLogo) 14.dp else 4.dp
        val tfFocusRequester = remember { FocusRequester() }

        TextField(
            placeholder = {
                Text(text = stringResource(R.string.sb_message)
                    .format(stringResource(serpProvider.providerInfo!!.titleInSearchBox)))
            },
            value = resultModel.query,
            shape = RoundedCornerShape(60.dp),
            colors = TextFieldDefaults.colors(
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            interactionSource = remember { MutableInteractionSource() }
                .also { interactionSource ->
                    LaunchedEffect(interactionSource) {
                        interactionSource.interactions.collect {
                            // If the field is pressed, hide logo
                            if (it is PressInteraction.Release)
                                resultModel.hideLogo()
                        }
                    }
                },
            modifier = Modifier
                .padding(
                    top = 4.dp, bottom = 4.dp,
                    start = tfSidePadding, end = tfSidePadding
                )
                .fillMaxWidth()
                .focusRequester(tfFocusRequester)
                .testTag(Global.TEST_TAG_RP_QUERY_FIELD),
            onValueChange = { nv ->
                resultModel.updateQuery(nv)
            },
            keyboardActions = KeyboardActions(
                onSearch = {
                    if (resultModel.query == "_gugal_devopts") {
                        val intent = Intent(context, DevOptMainActivity::class.java)
                        intent.flags = FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                    } else {
                        saveSearch(resultModel.query)
                        // Clear error response
                        error.value = null
                        resultModel.search()
                    }
                }
            ),
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            trailingIcon = {
                if (!resultState.showLogo)
                    IconButton(
                        onClick = {
                            resultModel.reset()
                        }
                    ) {
                        Icon(
                            painter = painterResource(R.drawable.ic_clear),
                            contentDescription = stringResource(R.string.btn_clear)
                        )
                    }
            }
        )

        // past search card
        AnimatedVisibility (resultState.showLogo,
            enter = fadeIn(),
            exit = fadeOut()) {
            Box(modifier = Modifier.fillMaxWidth()) {
               SearchHistory(onClick = { query ->
                   resultModel.updateQuery(query)
                   resultModel.search()
               }, fillOnClick = { query ->
                   resultModel.updateQuery(query)
                   // Focus on result field
                   tfFocusRequester.requestFocus()
               })
            }
        }

        // If a search query was passed, show it
        if (searchQuery.isNotEmpty() && lastSearchQuery != searchQuery) {
            lastSearchQuery = searchQuery
            resultModel.updateQuery(searchQuery)
            // Focus on result field
            LaunchedEffect(composeDone, block = {
                tfFocusRequester.requestFocus()
            })
        }

        // loader
        AnimatedVisibility (resultState.showLoadingBar) {
            LinearProgressIndicator(Modifier.fillMaxWidth())
        }

        // results list
        if (resultState.error == null) {
            AnimatedVisibility (!resultState.showLoadingBar,
                enter = fadeIn(),
                exit = fadeOut()) {
                Results(
                    results = resultState.results,
                    context,
                    resultState.sizeClass,
                    resultModel
                )
            }
        }
        // error response
        AnimatedVisibility (
            resultState.error != null,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            if (resultState.error!!.errorCode == "_NRR") ErrorMessage("")
            else ErrorMessage(resultState.error!!.body)
        }
    }

    composeDone.value = true
}

@Composable
fun PageButtons(resultModel: ResultViewModel) {
    val page = resultModel.getPage()
    Row (
        Modifier
            .padding(10.dp)
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Button(
            {resultModel.requestPrevPage()},
            enabled = page == 0
        ) {
            Text("<")
        }
        Text("Page $page")
        Button(
            {resultModel.requestNextPage()}
        ) {
            Text(">")
        }
    }
}

fun saveSearch(tsvt: String) {
    // Update the search histories
    fullSearchHistory.add(0, tsvt)
    visibleSearchHistory.add(0, tsvt)
    // Shorten the visible search history
    if (visibleSearchHistory.count() > maxSearchHistoryLength) {
        visibleSearchHistory = visibleSearchHistory.subList(0, maxSearchHistoryLength)
    }
    with (sharedPreferences.edit()) {
        this.putStringSet("pastSearches", fullSearchHistory.toSet())
        this.putString("visiblePastSearches", Gson().toJson(visibleSearchHistory))
        apply()
    }
}

@Composable
fun ErrorMessage(body: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (body.isNotEmpty()) {
            Text(
                stringResource(R.string.rp_error),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineSmall
            )
            Text(
                body,
                modifier = Modifier
                    .padding(all = 10.dp)
                    .fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        } else {
            Text(
                stringResource(R.string.rp_noresult),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineSmall
            )
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun SearchHistory(onClick: (String) -> Unit, fillOnClick: (String) -> Unit) {
    val searches: List<String> = if (Global.demoMode) Global.demoModeQueries.toList() else visibleSearchHistory.toList()
    Surface(
        shape = MaterialTheme.shapeScheme.large,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(start = 14.dp, end = 14.dp, top = 24.dp, bottom = 24.dp)
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        LazyColumn {
            items(searches) { message ->
                PastSearch(message, onClick = {onClick(message)}, fillOnClick = {fillOnClick(message)})
            }
        }
    }
}

@Composable
fun PastSearch(query: String, onClick: () -> Unit, fillOnClick: () -> Unit) {
    val fillActionDesc = stringResource(R.string.action_edit_past_search)
    val searchActionDesc = stringResource(R.string.action_do_past_search)
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
            .padding(top = 5.dp, bottom = 5.dp, start = 20.dp, end = 5.dp)
            .semantics {
                // Set any explicit semantic properties
                customActions = listOf(
                    CustomAccessibilityAction(searchActionDesc) {
                        onClick()
                        /*return*/ true
                    },
                    CustomAccessibilityAction(fillActionDesc) {
                        fillOnClick()
                        /*return*/ true
                    }
                )
            },
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = query,
            style = MaterialTheme.typography.titleLarge,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(2f)
        )
        IconButton(
            onClick = fillOnClick,
            modifier = Modifier.clearAndSetSemantics { }
        ) {
            Icon(painterResource(id = R.drawable.ic_fill_result), stringResource(R.string.btn_fill_result))
        }
    }
}