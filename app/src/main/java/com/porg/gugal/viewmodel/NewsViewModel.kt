/*
 *     NewsViewModel.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.viewmodel

import android.util.Log
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModel
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.demoModeArticles
import com.porg.gugal.news.Article
import com.rometools.rome.feed.synd.SyndEntry
import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.FeedException
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.io.IOException
import java.net.MalformedURLException
import java.util.Timer
import kotlin.concurrent.schedule

class NewsViewModel: ViewModel() {
    private val _uiState = MutableStateFlow(NewsState())
    val uiState: StateFlow<NewsState> = _uiState.asStateFlow()
    lateinit var q: RequestQueue
        private set

    private var feeds: List<String> = listOf()

    private var loadedFeeds = 0
    private var loading = false

    fun loadArticles(force: Boolean = false) {
        if (feeds.isEmpty()) {
            throw IllegalStateException("No feeds to load from. Please set the feeds with setFeeds(List<String>)")
        }
        if (loading && !force) {
            Log.w("gugalNews", "Not loading feeds as a load operation is in progress.")
            Log.w("gugalNews", "If you want to load feeds anyway, pass force=true.")
            return
        }

        loading = true
        Log.d("gugalNews", "Loading feeds...")

        if (Global.demoMode) {
            Timer("FeedFakeLoading", false).schedule(2500) {
                setArticles(demoModeArticles)
                loading = false
                _uiState.update {current ->
                    current.copy(loaded = true)
                }
            }
            return
        }

        // For each feed:
        for (feed in feeds) {
            Log.d("gugalNews", feed)
            // Request a string response from the provided URL.
            val stringRequest = StringRequest(
                Request.Method.GET, feed,
                { response ->
                    parseResponse(response)
                },
                { Log.e("gugalNews", "oops") }
            )
            // Add the request to the RequestQueue.
            q.add(stringRequest)
        }
    }

    private fun parseResponse(response: String) {
        val list = mutableListOf<Article>()
        try {
            // Parse the RSS feed
            val input = SyndFeedInput()
            val feed: SyndFeed = input.build(XmlReader(response.byteInputStream()))
            // Iterate over the RSS feed
            val iterator = feed.entries.listIterator()
            while (iterator.hasNext()) {
                val ent: SyndEntry = iterator.next() as SyndEntry
                var description = ent.description?.value ?: "(no description)"

                // Find a suitable article body
                val body = findBody(ent)

                // If the RSS has no description but has a body:
                description = if (body != "(unknown)" && description == "(no description)") {
                    // Get the body without HTML
                    val noHTMLBody = HtmlCompat.fromHtml(body,HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
                    // Get the first 100 characters and use that as the description
                    if (noHTMLBody.length > 100) noHTMLBody.substring(0, 100) + "..."
                    else noHTMLBody
                }
                // Else, remove any HTML from the description
                // TODO detect if the string has HTML before removing it
                else {
                    HtmlCompat.fromHtml(description,HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
                }

                // Build and add the storage
                val article = Article(
                    ent.title ?: "(no title)",
                    description,
                    ent.link ?: "(no link)",
                    ent.author ?: "(unknown)",
                    ent.contents,
                    ent.publishedDate,
                    body
                )
                list.add(article)
            }
            // Save the articles and increase the amount of loaded feeds
            setArticles(list)
            loadedFeeds++
            // Mark if everything is loaded
            if (loadedFeeds == feeds.size) {
                _uiState.update {current ->
                    current.copy(loaded = true)
                }
                // Re-enable loading feeds
                loading = false
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        } catch (e: FeedException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun findBody(ent: SyndEntry): String {
        // If the RSS has any contents:
        if (ent.contents.isNotEmpty()) {
            // The first HTML content is the body
            for (content in ent.contents) {
                if (content.type.endsWith("html"))
                    return content.value
            }
        }
        // If it doesn't have contents, but has a description, use that as the body
        else if (ent.description != null) {
            return ent.description.value
        }
        // Else, we don't have a suitable tag to use as the body.
        return "(unknown)"
    }

    fun setQueue(queue: RequestQueue) {
        q = queue
    }

    fun setArticles(articles: List<Article>) {
        _uiState.update {current ->
            current.copy(articles = _uiState.value.articles + articles)
        }
    }

    fun loadArticlesIfNecessary() {
        if (_uiState.value.articles.isEmpty()) loadArticles()
    }

    fun setFeeds(feedList: List<String>) {
        feeds = feedList
    }
}