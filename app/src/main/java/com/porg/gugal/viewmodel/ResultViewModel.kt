/*
 *     ResultViewModel.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.viewmodel

import android.content.Context
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.porg.gugal.Global
import com.porg.gugal.data.Result
import com.porg.gugal.providers.responses.ErrorResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class ResultViewModel: ViewModel() {
    private val _uiState = MutableStateFlow(ResultState())
    val uiState: StateFlow<ResultState> = _uiState.asStateFlow()

    lateinit var queue: RequestQueue
        private set

    var query by mutableStateOf("")
        private set

    fun showOrHideLogo(newValue: Boolean) {
        _uiState.update {current ->
            current.copy(showLogo = newValue)
        }
    }

    fun showLogo() {showOrHideLogo(true)}
    fun hideLogo() {showOrHideLogo(false)}


    fun showOrHideLoadingBar(newValue: Boolean) {
        _uiState.update {current ->
            current.copy(showLoadingBar = newValue)
        }
    }

    fun showLoadingBar() {showOrHideLoadingBar(true)}
    fun hideLoadingBar() {showOrHideLoadingBar(false)}

    fun updateQuery(newQuery: String) {
        if (_uiState.value.showLogo) hideLogo()
        query = newQuery
    }

    fun createQueue(context: Context) {
        queue = Volley.newRequestQueue(context)
    }

    fun search() {
        showLoadingBar()
        setError(null)
        // Search using the current SERP provider
        val req = Global.serpProvider.search(
            query,
            { res ->
                hideLoadingBar()
                setResults(res)
            },
            { err ->
                hideLoadingBar()
                setError(err)
            }
        )
        if (req != null) queue.add(req)
    }

    fun clearResults() {
        _uiState.update {current ->
            current.copy(
                results = listOf(),
                page = 0,
                pageCount = 0
            )
        }
    }

    fun setResults(results: List<Result>) {
        _uiState.update {current ->
            current.copy(results = results)
        }
    }

    fun setError(error: ErrorResponse?) {
        _uiState.update {current ->
            current.copy(error = error)
        }
    }


    fun updateSizeClass(windowSizeClass: WindowWidthSizeClass) {
        _uiState.update {current ->
            current.copy(sizeClass = windowSizeClass)
        }
    }

    fun reset() {
        _uiState.update {current ->
            current.copy(
                showLogo = true,
                results = listOf(),
                page = 0,
                pageCount = 0
            )
        }
        query = ""
    }

    fun getPage(): Int {
        return _uiState.value.page
    }

    fun requestNextPage() {
        _uiState.update {current ->
            current.copy(
                page = _uiState.value.page + 1
            )
        }
        search()
    }

    fun requestPrevPage() {
        _uiState.update {current ->
            current.copy(
                page = _uiState.value.page - 1
            )
        }
        search()
    }
}