/*
 *     ResultState.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.viewmodel
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import com.porg.gugal.data.Result
import com.porg.gugal.providers.responses.ErrorResponse

data class ResultState constructor(
    val showLogo: Boolean = true,
    val showLoadingBar: Boolean = false,
    val query: String = "",
    val results: List<Result> = listOf(),
    val page: Int = 0,
    val pageCount: Int = 0,
    val sizeClass: WindowWidthSizeClass = WindowWidthSizeClass.Compact,
    val error: ErrorResponse? = null
)