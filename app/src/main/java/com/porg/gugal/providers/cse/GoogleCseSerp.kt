/*
 *     GoogleCseSerp.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers.cse

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.porg.gugal.R
import com.porg.gugal.data.Result
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.exceptions.InvalidCredentialException
import com.porg.gugal.providers.responses.ErrorResponse
import com.porg.gugal.providers.responses.InvalidCredentialResponse
import com.porg.gugal.providers.responses.NoResultsResponse
import org.json.JSONObject
import java.nio.charset.Charset

class GoogleCseSerp: SerpProvider {
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun ConfigComposable(
        modifier: Modifier,
        enableNextButton: MutableState<Boolean>,
        context: Context
    ) {
        val _cx = remember { mutableStateOf(TextFieldValue()) }
        val _ak = remember { mutableStateOf(TextFieldValue()) }
        Column(
            modifier = modifier
        ) {
            Text(
                text = stringResource(R.string.serp_gcse_setup_1),
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            TextField(
                placeholder = { Text(text = stringResource(R.string.serp_gcse_cx)) },
                value = _cx.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _cx.value = nv
                    cx = _cx.value.text
                    enableNextButton.value = apikey.isNotEmpty() && cx.isNotEmpty()
                },
                keyboardActions = KeyboardActions(
                    onDone = {
                        cx = _cx.value.text
                        enableNextButton.value = apikey.isNotEmpty() && cx.isNotEmpty()
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = stringResource(R.string.serp_gcse_setup_2),
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            TextField(
                placeholder = { Text(text = stringResource(R.string.serp_gcse_apiKey)) },
                value = _ak.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _ak.value = nv
                    apikey = nv.text
                    enableNextButton.value = apikey.isNotEmpty() && cx.isNotEmpty()
                },
                keyboardActions = KeyboardActions(
                    onDone = {
                        apikey = _ak.value.text
                        enableNextButton.value = apikey.isNotEmpty() && cx.isNotEmpty()
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = stringResource(R.string.serp_gcse_setup_3),
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }

    override fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("ak" to apikey, "cx" to cx)
    }

    override fun useSensitiveCredentials(credentials: Map<String, String>) {
        if (!credentials.containsKey("cx") || !credentials.containsKey("ak")) throw InvalidCredentialException()
        cx = credentials["cx"]!!
        apikey = credentials["ak"]!!
    }

    override fun getSensitiveCredentialNames(): Array<String> {
        return arrayOf("cx", "ak")
    }

    override fun search(
        query: String,
        setResults: (List<Result>) -> Unit,
        setError: (ErrorResponse) -> Unit
    ): JsonObjectRequest? {
        val url = "https://customsearch.googleapis.com/customsearch/v1?cx=$cx&key=$apikey&q=$query"

        // Request a string response from the provided URL.
        return JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // Send no results response if there are no results
                if (!response.has("items")) setError(NoResultsResponse())
                else {
                    val items = response.getJSONArray("items")
                    val list: MutableList<Result> = mutableListOf()
                    for (i in 0 until items.length()) {
                        val item: JSONObject = items.getJSONObject(i)
                        if (item.has("snippet"))
                            list.add(
                                Result(
                                    item.getString("title"), item.getString("snippet"),
                                    item.getString("link"), item.getString("displayLink")
                                )
                            )
                        else
                            list.add(
                                Result(
                                    item.getString("title"), null,
                                    item.getString("link"), item.getString("displayLink")
                                )
                            )
                    }
                    setResults(list)
                }
            },
            { error ->
                // Retrieve body, if it exists
                if (error.networkResponse.data != null) {
                    // Detect credential error and use an invalid credential response
                    val response = String(error.networkResponse.data, Charset.forName("UTF-8"))
                    setError(
                        if ("API_KEY_INVALID" in response
                            || "INVALID_ARGUMENT" in response
                            || "PERMISSION_DENIED" in response) InvalidCredentialResponse()
                        // Else use a regular error response
                        else ErrorResponse(response, error.networkResponse.statusCode.toString())
                    )
                } else {
                    setError(ErrorResponse("General error", error.networkResponse.statusCode.toString()))
                }
            }
        )
    }

    private var apikey = ""
    private var cx = ""

    override val id: String get() = Companion.id
    override val providerInfo = Companion.providerInfo

    companion object {
        val id: String = "abf621d5a5df4e4ba2ea70dd9362f1a3-goog"
        val providerInfo = ProviderInfo(
            R.string.serp_gcse_title,
            R.string.serp_gcse_desc,
            R.string.serp_gcse_sb,
            true)
    }
}