/*
 *     ShareActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi

class ShareActivity: ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // links shared to Gugal
        if (intent?.action == Intent.ACTION_SEND && "text/plain" == intent.type) {
            intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
                openGugal(it)
            }
        }
        // context menu
        else if (intent?.action == Intent.ACTION_PROCESS_TEXT && "text/plain" == intent.type) {
            intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT)?.let {
                openGugal(it as String)
            }
        }
        // search engine links
        else if (intent?.action == Intent.ACTION_VIEW) {
            val uri: Uri? = intent?.data
            if (uri != null) {
                Log.d("gugal", uri.toString())
                // Handle empty links that some launchers, like Lawnchair, send
                if (uri.toString().endsWith(".com/") || uri.toString().endsWith(".local/")) openGugal()
                // Handle queries
                if (uri.host?.contains("ogle") == true ||
                    uri.host?.contains("bin") == true ||
                    uri.host?.contains("gugal") == true) {
                    val q = uri.getQueryParameter("q")
                    if (q != null) {
                        openGugal(q)
                    }
                }
            }
        }
        finish()
    }

    @OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
    private fun openGugal(query: String = "") {
        val searchIntent = Intent(this, MainActivity::class.java)
        if (query.isNotEmpty()) searchIntent.putExtra("query", query)
        startActivity(searchIntent)
    }
}