/*
 *     NSViewModel.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.news

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.demoModeEditorial
import com.porg.gugal.SUGGESTED_FEED_LIST_URL
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import org.json.JSONObject

data class Feed(val url: String, val name: String)

data class NSState constructor(
    val feeds: List<Feed> = listOf()
)

class NSViewModel: ViewModel() {

    private val _uiState = MutableStateFlow(NSState())
    val uiState: StateFlow<NSState> = _uiState.asStateFlow()
    lateinit var q: RequestQueue
        private set

    fun loadFeeds() {

        // Request a string response from the provided URL.
        val stringRequest = JsonObjectRequest(
            SUGGESTED_FEED_LIST_URL,
            { response ->
                if (Global.demoMode) {
                    setFeeds(demoModeEditorial)
                } else {
                    val items = response.getJSONArray("feeds")
                    val list: MutableList<Feed> = mutableListOf()
                    for (i in 0 until items.length()) {
                        val item: JSONObject = items.getJSONObject(i)
                        list.add(Feed(item["url"] as String, item["name"] as String))
                    }
                    setFeeds(list)
                }
            },
            { Log.e("gugalNews", "NewsSettings error!") })

        // Add the request to the RequestQueue.
        q.add(stringRequest)
    }

    fun createQueue(context: Context) {
        q = Volley.newRequestQueue(context)
    }

    fun setFeeds(feeds: List<Feed>) {
        _uiState.update {current ->
            current.copy(feeds = feeds)
        }
    }
}