/*
 *     NewsSettingsActivity.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.news

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import com.porg.gugal.Global
import com.porg.gugal.MainActivity
import com.porg.gugal.NEWS_ISSUE_URL
import com.porg.gugal.R
import com.porg.gugal.open
import com.porg.gugal.setup.SetupNewsActivity
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.MainSwitch
import com.porg.m3.Tip
import com.porg.m3.settings.RegularSetting

@OptIn(ExperimentalMaterial3Api::class)
class NewsSettingsActivity: ComponentActivity() {
    @OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val that = this
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
            val mainSwitchChecked = remember {mutableStateOf(
                Global.sharedPreferences.getBoolean("newsPreview", false)
            )}
            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setting_news_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background) {
                            Column(modifier = Modifier
                                .padding(innerPadding)
                                .verticalScroll(rememberScrollState())
                                .fillMaxSize()
                            ) {
                                MainSwitch(title = getString(R.string.setting_news_toggle),
                                    checked = mainSwitchChecked.value,
                                    onCheckedChange = { t ->
                                        // update checked value
                                        mainSwitchChecked.value = t
                                        // update shared preference
                                        with (Global.sharedPreferences.edit()) {
                                            putBoolean("newsPreview", t)
                                            apply()
                                        }
                                        Global.gugalNews.enabled = t
                                        // start news setup if there are no feeds
                                        if ((Global.sharedPreferences.getStringSet(
                                                "newsFeeds", null
                                            )?.size ?: 0) == 0 /*&&*/|| t
                                        ) {
                                            startNewsSetup(that)
                                        } else {
                                            // restart main activity
                                            startActivity(Intent(that, MainActivity::class.java))
                                        }
                                    }
                                )
                                Text(
                                    text = getText(R.string.news_body).toString(),
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp, vertical = 8.dp),
                                    style = MaterialTheme.typography.bodyLarge
                                )
                                Tip(
                                    text = getString(R.string.news_body_preview),
                                    modifier = Modifier.padding(
                                        top = 24.dp,
                                        start = 16.dp,
                                        end = 16.dp
                                    ).fillMaxWidth(), icon = R.drawable.ic_info,
                                    onClick = {  }
                                )
                                RegularSetting(
                                    stringResource(R.string.setting_newsFeeds_title),
                                    stringResource(R.string.setting_newsFeeds_desc),
                                    onClick = {startNewsSetup(that)})
                                RegularSetting(
                                    R.string.setting_issue_title,
                                    R.string.setting_newsIssue_desc,
                                    onClick = { open(that, NEWS_ISSUE_URL) }
                                )
                            }
                        }
                    }
                )
            }
        }
    }

    fun startNewsSetup(that: Context) {
        val intent = Intent(that, SetupNewsActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra("restartMain", true)
        ContextCompat.startActivity(that, intent, null)
    }
}