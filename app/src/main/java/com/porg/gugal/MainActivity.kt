/*
 *     MainActivity.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.gson.Gson
import com.porg.gugal.Global.Companion.fullSearchHistory
import com.porg.gugal.Global.Companion.maxSearchHistoryLength
import com.porg.gugal.Global.Companion.serpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.Global.Companion.visibleSearchHistory
import com.porg.gugal.news.NewsPage
import com.porg.gugal.providers.exceptions.InvalidCredentialException
import com.porg.gugal.setup.SetupNewStartActivity
import com.porg.gugal.setup.SetupStartActivity
import com.porg.gugal.ui.ResultPage
import com.porg.gugal.ui.SettingsPage
import com.porg.gugal.ui.theme.GugalTheme
import org.json.JSONArray

@ExperimentalAnimationApi
@ExperimentalMaterialApi
class MainActivity : ComponentActivity() {

    private var apikey = ""
    private var cx = ""
    private var ca: List<String>? = null
    private lateinit var context: Activity
    private var invalidCredentials: Boolean = false

    private var showLoadOldPrefsAlert = false
    private var showSetup = false
    private var searchQuery = ""

    @ExperimentalAnimationApi
    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        context = this

        // Retrieve shared search query
        val query = intent.getStringExtra("query")
        var sender = ""
        if (referrer != null) sender = referrer!!.host.toString()
        // Don't allow queries from other apps
        if (query != null && sender == applicationContext.packageName) {
            searchQuery = query
        }

        // Create global sharedPreferences object
        Global.createSharedPreferences(applicationContext)

        // Load SERP provider
        loadSerp()
        // Load search history
        loadPastSearches()
        // Update some experiments
        Global.gugalNews.enabled = sharedPreferences.getBoolean("newsPreview", false)
        // Show setup wizard if necessary
        if (showSetup) {
            showSetup = false
            startActivity(Intent(applicationContext, SetupNewStartActivity::class.java))
            return
        }

        ca = loadCxApi()
        if (serpProvider.id.endsWith("-goog") && (ca?.size ?: 0) == 2) {
            if ((ca?.get(0) ?: "none") != "none") cx = ca?.get(0) ?: ""
            if ((ca?.get(1) ?: "none") != "none") apikey = ca?.get(1) ?: ""
        }

        var startIntent: Intent? = null
        val noCredentials = ca.isNullOrEmpty() || invalidCredentials
        // the changelog shouldn't be shown on first run
        if (noCredentials && serpProvider.providerInfo?.requiresSetup ?: noCredentials) {
            startIntent = Intent(context, SetupStartActivity::class.java)
        } else if (shouldShowChangelog()) {
            startIntent = Intent(context, ChangelogActivity::class.java)
        }
        if (startIntent != null) {
            startIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            ContextCompat.startActivity(context, startIntent, null)
        }

        val simple = resources.getBoolean(R.bool.isSimple) || intent.getBooleanExtra("simple", false)
        setContent {
            val windowSizeClass = calculateWindowSizeClass(this)
            GugalTheme {
                if (showLoadOldPrefsAlert) {
                    Column(modifier = Modifier.fillMaxWidth().fillMaxHeight(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally) {
                        Text("Updating save data...", Modifier.padding(bottom = 16.dp))
                        LinearProgressIndicator()
                    }
                    resaveGoogleNonSerpPrefs(cx, apikey)
                } else {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        color = MaterialTheme.colorScheme.background
                    ) {
                        if (simple)
                            ResultPage(
                                context, searchQuery,
                                windowSizeClass = windowSizeClass
                            )
                        else {
                            val navController = rememberNavController()
                            // If the device is a phone in vertical, use a vertical navigation bar.
                            if (windowSizeClass.widthSizeClass == WindowWidthSizeClass.Compact) {
                                Scaffold(
                                    content = { padding ->
                                        NavigationHost(
                                            navController = navController,
                                            padding = padding,
                                            sizeClass = windowSizeClass
                                        )
                                    },
                                    bottomBar = { BottomNavigationBar(navController = navController) },
                                    backgroundColor = Color.Transparent
                                )
                            }
                            // Otherwise use a navigation rail.
                            else {
                                Row(modifier = Modifier.systemBarsPadding()){
                                    SideNavigationRail(navController)
                                    NavigationHost(
                                        navController = navController,
                                        padding = PaddingValues(5.dp),
                                        sizeClass = windowSizeClass
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private fun shouldShowChangelog(): Boolean {
        try {
            // if there is no lastGugalVersion preference the last version is older than 0.5
            if (!sharedPreferences.contains("lastGugalVersion")) {
                with(sharedPreferences.edit()) {
                    this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
                    apply()
                    return true
                }
            }

            if (sharedPreferences.getInt("lastGugalVersion",
                    BuildConfig.VERSION_CODE) < BuildConfig.VERSION_CODE
            ) {
                with(sharedPreferences.edit()) {
                    this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
                    apply()
                    return true
                }
            } else return false
        }
        // if any errors occur reading/writing to the preferences, don't show the changelog
        catch (exc: Exception) {
            return false
        }
    }

    // Loads the SERP provider.
    private fun loadSerp() {
        val serpID = sharedPreferences.getString("serp", "none")
        Log.d("gugal", "loadSerp: serp ID is $serpID")

        if (serpID != "none") Global.setSerpProvider(serpID!!)
        else showSetup = true
    }

    // Loads the search history.
    fun loadPastSearches() {
        // Load the full search history
        val ps = sharedPreferences.getStringSet("pastSearches", mutableSetOf())
        if (ps != null) fullSearchHistory = ps.toMutableList()
        // Try to load visible search history
        val vpsj = sharedPreferences.getString("visiblePastSearches", "[]")
        if (vpsj != "[]") {
            val vps = JSONArray(vpsj)
            visibleSearchHistory = vps.toMutableStringList()
        } else {
            // If there is no visible history, load the first N from the full history and save them
            visibleSearchHistory =
                if (fullSearchHistory.count() < maxSearchHistoryLength) fullSearchHistory
                else fullSearchHistory.subList(0, maxSearchHistoryLength)
            with (sharedPreferences.edit()) {
                this.putString("visiblePastSearches", Gson().toJson(visibleSearchHistory))
                apply()
            }
        }
    }

    private fun JSONArray.toMutableStringList(): MutableList<String> = MutableList(length(), this::getString)

    private fun resaveGoogleNonSerpPrefs(cx: String, apikey: String) {
        with (sharedPreferences.edit()) {
            // Edit the user's shared preferences...
            this.putString("serp_${serpProvider.id}_cx", cx)
            this.putString("serp_${serpProvider.id}_ak", apikey)
            // ...and remove the old preferences
            this.remove("serp_google_data_cx")
            this.remove("serp_google_data_ak")
            apply()
        }
        showLoadOldPrefsAlert = false
    }

    private fun loadCxApi(): List<String> {
        val arr: ArrayList<String> = ArrayList()
        // load pre-0.4 prefs if the SERP provider is google and they exist
        if (sharedPreferences.getString("serp_google_data_cx", "none") != "none" && serpProvider.id.endsWith("-goog")) {
            sharedPreferences.getString("serp_google_data_cx", "none")?.let { arr.add(it) }
            sharedPreferences.getString("serp_google_data_ak", "none")?.let { arr.add(it) }
            showLoadOldPrefsAlert = true
            return arr.toList()
        }

        val crdMap: MutableMap<String, String> = mutableMapOf()
        // request the sensitive cred names from the SERP provider and load them
        for (name in serpProvider.getSensitiveCredentialNames())
            sharedPreferences.getString("serp_${serpProvider.id}_${name}", "none")?.let { if (it != "none") {arr.add(it); crdMap[name] = it} }
        // pass the credentials to the SERP provider
        try {
            serpProvider.useSensitiveCredentials(crdMap)
        }
        // if the SERP provider notifies the app that they are incorrect, re-run setup
        catch (x: InvalidCredentialException) {
            Log.w("Gugal", "Received invalid credentials, should re-run setup.")
            invalidCredentials = true
        }
        // if the SERP provider gives any other exception, it's broken and we should ask for another one
        catch (x: Exception) {
            Log.w("Gugal", "Received exception, should re-run setup.")
            invalidCredentials = true
        }
        return arr.toList()
    }

    @Composable
    fun BottomNavigationBar(navController: NavHostController) {

        NavigationBar {
            val backStackEntry by navController.currentBackStackEntryAsState()
            val currentRoute = backStackEntry?.destination?.route

            var navItems = NavBarItems.BarItems
            if (Global.gugalNews.enabled) navItems = NavBarItems.BarItemsWithNews

            navItems.forEach { navItem ->

                NavigationBarItem(
                    selected = currentRoute == navItem.route,
                    onClick = {
                        navController.navigate(navItem.route) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },
                    modifier = Modifier.testTag("MA_NI_" + navItem.route),

                    icon = {
                        Icon(imageVector = ImageVector.vectorResource(navItem.image),
                            contentDescription = getText(navItem.title).toString())
                    },
                    label = {
                        Text(text = getText(navItem.title).toString())
                    },
                )
            }
        }
    }
    @Composable
    fun SideNavigationRail(navController: NavHostController) {

        NavigationRail {
            val backStackEntry by navController.currentBackStackEntryAsState()
            val currentRoute = backStackEntry?.destination?.route

            var navItems = NavBarItems.BarItems
            if (Global.gugalNews.enabled) navItems = NavBarItems.BarItemsWithNews

            navItems.forEach { navItem ->

                NavigationRailItem(
                    selected = currentRoute == navItem.route,
                    onClick = {
                        navController.navigate(navItem.route) {
                            popUpTo(navController.graph.findStartDestination().id) {
                                saveState = true
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    },

                    icon = {
                        Icon(imageVector = ImageVector.vectorResource(navItem.image),
                            contentDescription = getText(navItem.title).toString())
                    },
                    label = {
                        Text(text = getText(navItem.title).toString())
                    },

                    modifier = Modifier.padding(5.dp)
                )
            }
        }
    }

    @Composable
    fun NavigationHost(
        navController: NavHostController,
        padding: PaddingValues,
        modifier: Modifier = Modifier,
        sizeClass: WindowSizeClass
    ) {
        NavHost(
            navController = navController,
            startDestination = Routes.Search.route,
            modifier = Modifier.padding(padding).statusBarsPadding().then(modifier)
        ) {
            composable(Routes.Search.route) {
                ResultPage(
                    context,
                    searchQuery,
                    windowSizeClass = sizeClass
                )
            }

            composable(Routes.Settings.route) {
                SettingsPage(context)
            }

            composable(Routes.News.route) {
                NewsPage(context)
            }
        }
    }
}