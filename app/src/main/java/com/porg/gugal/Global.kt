/*
 *     Global.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.porg.gugal.devopt.providers.AlwaysFailingProvider
import com.porg.gugal.news.Article
import com.porg.gugal.news.Feed
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.providers.searx.SearXSerp
import java.util.Date

class Global {
    companion object {
        var visibleSearchHistory: MutableList<String> = mutableListOf()
        var fullSearchHistory: MutableList<String> = mutableListOf()
        lateinit var serpProvider: SerpProvider
        // TODO if adding SERP providers, add your provider in the function and array below
        val allSerpProviders: Array<String> = arrayOf(
            GoogleCseSerp.id,
            SearXSerp.id
        )
        fun setSerpProvider(serpID: String) {
            when (serpID) {
                GoogleCseSerp.id -> serpProvider = GoogleCseSerp()
                SearXSerp.id -> serpProvider = SearXSerp()
                // Check if serpID matches your SERP provider's ID, and if so set Global.serpProvider to
                // an instance of your SERP provider.
                AlwaysFailingProvider.id -> serpProvider = AlwaysFailingProvider()
            }
        }

        private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        private val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)
        const val sharedPrefsFile = "gugalprefs"
        lateinit var sharedPreferences: SharedPreferences

        fun createSharedPreferences(appContext: Context) {
            sharedPreferences = EncryptedSharedPreferences.create(
                sharedPrefsFile,
                mainKeyAlias,
                appContext,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        }

        // Experiments
        var gugalNews = Experiment("News", false)
        var custTabInResults = Experiment("Custom tabs for results", false)
        var resultGrid = Experiment("Use grid for results (tablets only)", false)
        var searchPages = Experiment("Search result pagination", false)
        var gugalNewsSetup = Experiment("News - Setup pages", false, gugalNews)

        // Magic numbers
        const val THEME_SYSTEM = 0
        const val THEME_LIGHT = 1
        const val THEME_DARK = 2

        // Demo mode
        var demoMode = false
        val demoModeQueries = arrayOf(
            "Gugal",
            "open-source software",
            "Android privacy",
            "libre software",
            "degoogling"
        )
        val demoModeEditorial = listOf(
            Feed("https://www.libre.bee/rss/feed.xml","The Libre Bee"),
            Feed("https://www.aday.later/feed.xml","A Day Later"),
            Feed("https://androidgovt.phone/feed/atom","Android Government"),
            Feed("https://www.news.room/rss/world.xml","The Newsroom - World News"),
            Feed("https://www.news.room/rss/pol.xml","The Newsroom - Politics")
        )
        val demoModeArticles = listOf(
            Article(
                "The next Android version is here",
                "With even more 'helpful' generative AI features and less useful features, this version is sure to be a hit.",
                "https://news.example.com/stories/2023/04/10/android",
                "Jade Smith",
                mutableListOf(),
                Date(2023, 4, 10),
                "This week, we came across <i>Gugal</i>. This app describes itself as '[a] clean, lightweight FOSS search app."
            ),
            Article(
                "How to reduce your digital footprint",
                "Tech giants are trying to destroy your privacy - let's prevent that as much as we can with these open-source, free apps.",
                "https://news.example.com/stories/2023/04/10/android",
                "Adam Butcher (The Libre Bee)",
                mutableListOf(),
                Date(2023, 4, 10),
                "This week, we came across <i>Gugal</i>. This app describes itself as '[a] clean, lightweight FOSS search app."
            ),
            Article(
                "Gugal - an open source search client",
                "Reviewing an open source app that helps you google without the Google app.",
                "https://news.example.com/stories/2023/01/01/gugal",
                "John Smith (The Libre Bee)",
                mutableListOf(),
                Date(2023, 4, 9),
                "This week, we came across <i>Gugal</i>. This app describes itself as '[a] clean, lightweight FOSS search app."
            ),
            Article(
                "Global Cooperation Leads to Progress on Key Development Goals - The Newsroom",
                "Despite ongoing challenges, the global community made significant strides in addressing key development goals over the past year. From reducing poverty and hunger to improving education...\n" +
                        "(read the rest of the article online)",
                "https://news.example.com/stories/2023/04/09/development",
                "Arthur I.",
                mutableListOf(),
                Date(2023, 4, 9),
                "This week, we came across <i>Gugal</i>. This app describes itself as '[a] clean, lightweight FOSS search app."
            )
        )

        // Tests
        const val TEST_TAG_RP_QUERY_FIELD: String = "RP_QueryField"
        const val TEST_TAG_MA_SETTINGS = "MA_Settings"

        // Settings
        var maxSearchHistoryLength = 5
    }
}

fun open(context: Context, url: String) {
    val builder = CustomTabsIntent.Builder()
    // show website title
    builder.setShowTitle(true)
    // animation for enter and exit of tab
    builder.setStartAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
    builder.setExitAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
    // launch the passed URL
    builder.build().launchUrl(context, Uri.parse(url))
}