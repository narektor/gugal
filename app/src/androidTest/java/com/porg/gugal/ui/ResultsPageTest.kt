/*
 *     ResultsPageTest.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.util.Log
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.porg.gugal.Global
import com.porg.gugal.MainActivity
import com.porg.gugal.Words.Companion.listOfWords
import com.porg.gugal.providers.DummySerp
import org.junit.Rule
import org.junit.Test

@OptIn(
    ExperimentalAnimationApi::class,
    ExperimentalMaterialApi::class
)
class ResultsPageTest {

    @get:Rule
    val mainActivityRule = createAndroidComposeRule<MainActivity>()

    val TAG = "gugalTests"

    private fun setup() {
        // Set a dummy SERP provider
        Global.serpProvider = DummySerp()
    }

    private fun searchFor(query: String) {
        mainActivityRule.onNodeWithTag(Global.TEST_TAG_RP_QUERY_FIELD).performTextClearance()
        mainActivityRule.onNodeWithTag(Global.TEST_TAG_RP_QUERY_FIELD).performTextInput(query)
        mainActivityRule.onNodeWithTag(Global.TEST_TAG_RP_QUERY_FIELD).performImeAction()
    }

    @Test
    fun saveSearch() {
        // Setup the app for testing.
        setup()
        // Make sure the visible search history is limited to 5 items.
        Global.maxSearchHistoryLength = 5
        // Clear the search history, if there is any.
        if (Global.visibleSearchHistory.isNotEmpty()) {
            mainActivityRule.onNodeWithTag("MA_NI_settings").performClick()
            mainActivityRule.onNodeWithText("Clear search history").performClick()
            mainActivityRule.onNodeWithText("Yes").performClick()
            // Return to the search page.
            mainActivityRule.onNodeWithTag("MA_NI_search").performClick()
        }
        // Get a list of 15 words.
        val words = listOfWords()
        // Run the first 5 queries first.
        var words5 = words.subList(0, 5)
        for (query in words5) {
            searchFor(query)
        }
        // Clear the search.
        mainActivityRule.onNodeWithContentDescription("Clear").performClick()
        // Now, check the results list.
        Log.d(TAG, "==========================================")
        Log.d(TAG,"expected:"+words5.reversed())
        Log.d(TAG,"actual:"+Global.visibleSearchHistory.toList())
        assert(Global.visibleSearchHistory.toList() == words5.reversed())
        Log.d(TAG, "==========================================")

        // Switch between pages a few times to cause the page to be refreshed.
        for (i in 0..10) {
            if (i % 2 == 0)
                mainActivityRule.onNodeWithTag("MA_NI_settings").performClick()
            else
                mainActivityRule.onNodeWithTag("MA_NI_search").performClick()
        }
        // Make sure that we are on the search page.
        mainActivityRule.onNodeWithTag("MA_NI_search").performClick()
        // Run the next 5 words.
        words5 = words.subList(5, 10)
        for (query in words5) {
            searchFor(query)
        }
        // Clear the search.
        mainActivityRule.onNodeWithContentDescription("Clear").performClick()
        // Now, check the results list.
        Log.d(TAG, "==========================================")
        Log.d(TAG,"expected:"+words5.reversed())
        Log.d(TAG,"actual:"+Global.visibleSearchHistory.toList())
        assert(Global.visibleSearchHistory.toList() == words5.reversed())
        Log.d(TAG, "==========================================")

        // Close the Search page.
        mainActivityRule.onNodeWithTag("MA_NI_settings").performClick()
        // Reload the search list.
        mainActivityRule.activity.loadPastSearches()
        // Return to the search page, and run the last 5 words.
        mainActivityRule.onNodeWithTag("MA_NI_search").performClick()
        words5 = words.subList(10, 15)
        for (query in words5) {
            searchFor(query)
        }
        // Clear the search.
        mainActivityRule.onNodeWithContentDescription("Clear").performClick()
        // Now, check the results list.
        Log.d(TAG, "==========================================")
        Log.d(TAG,"expected:"+words5.reversed())
        Log.d(TAG,"actual:"+Global.visibleSearchHistory.toList())
        assert(Global.visibleSearchHistory.toList() == words5.reversed())
        Log.d(TAG, "==========================================")
    }
}