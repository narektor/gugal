# Module app

Welcome to the SERP provider API reference.

# Package com.porg.m3

Material 3-style components that aren't included in Google's official 
library, designed to be consistent with those components.

# Package com.porg.m3.settings

Material 3-style components to be used in settings pages.

# Package com.porg.gugal.data

Various data classes shared by SERP providers, like results.

# Package com.porg.gugal.providers

Pretty much everything that has to do with SERP providers - including the 
SERP provider interface itself.

# Package com.porg.gugal.providers.exceptions

Deprecated in favor of error responses (see `com.porg.gugal.providers.responses`).

# Package com.porg.gugal.providers.responses

Can be used to tell Gugal (and the user) that something has gone wrong.