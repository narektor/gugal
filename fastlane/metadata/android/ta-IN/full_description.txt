குகல் ஒரு தூய்மையான மற்றும் இலகுரக வலை தேடல் பயன்பாடாகும், இது நவீன, பொருள் 3/நீங்கள் இடைமுகம் உடன் உள்ளது.

 இது 2 தேடுபொறிகளை ஆதரிக்கிறது - கூகிள் மற்றும் சீர்க்ச்/சியர்ச்ங். உருவாக்குபவர்கள் அதிக தேடுபொறிகளைச் சேர்க்க இலவசம்.

 கூகிள் தேடலுடன் குகலைப் பயன்படுத்த உங்களுக்கு சிஎச்இ ஐடி மற்றும் பநிஇ விசை தேவை. இரண்டையும் பெறுவதற்கான வழிமுறைகளை பயன்பாட்டில் காணலாம். <b> இவற்றைப் பெற நீங்கள் Google கணக்கைக் கொண்டிருக்க வேண்டும், எனவே கூகிள் இன்னும் உங்கள் கணக்கில் தேடல்களை இணைக்கக்கூடும். உங்கள் தனியுரிமையை மேலும் மதிக்கும் ஒரு தேடுபொறியைப் பயன்படுத்த விரும்பினால், குகலைப் ஒரு சீர்க்ச்/சியர்ச்ங் நிகழ்வைப் பயன்படுத்தவும். </B>

 <i> கூகிள் மற்றும் கூகிள் தேடல் கூகிள் எல்.எல்.சியின் வர்த்தக முத்திரைகள். </i>
