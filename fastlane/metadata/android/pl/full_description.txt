Gugal to przejszysta i lekka aplikacja do wyszukiwania stron internetowych. Posiada nowoczesny motyw Material 3/You.

Obsługuje dwie wyszukiwarki - Google i searx/SearXNG. Deweloperzy mogą swobodnie dodawać kolejne wyszukiwarki.

Aby używać Gugala z wyszukiwarką Google, potrzebujesz CSE ID i klucza API. Instrukcje dotyczące uzyskania obu można znaleźć w aplikacji. <b>Aby je uzyskać, musisz mieć konto Google, dlatego Google może nadal łączyć wyszukiwania z Twoim kontem. Jeśli chcesz używać wyszukiwarki, która bardziej szanuje Twoją prywatność, użyj Gugala z instancją searx/SearXNG.</b>

<i>Google i Google Search są znakami towarowymi Google LLC.</i>
