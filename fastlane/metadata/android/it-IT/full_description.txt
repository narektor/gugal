Gugal è un'app di ricerca web pulita e leggera, con un'interfaccia moderna, che sfrutta Materia 3/You UI.

Supporta due motori di ricerca - Google e SearX/SearXNG. Gli sviluppatori sono liberi di aggiungere più motori di ricerca a quelli supportati.

Per usare Gugal con Google, bisogna ottenere un ID CSE ed una chiave Api per accesso programmatico. Nell'app troverai le istruzioni per ottenere queste informazioni. <b>Per queste informazioni necessiterai di un account Google, rischiando che le ricerche fatte vengano legate al tuo account. Se cerchi un motore di ricerca che rispetti la privacy, prova Gugal con SearX.</b>

<i>Google e Google Search sono marchi registrati di Google LLC.</i>
