Gugal은 Jetpack Compose로 만든 깔끔하고 가벼운 웹 검색 앱입니다. 그것은 현대적인 Material 3/You UI를 가지고 있습니다.

그것은 2개의 검색 엔진을 지원합니다 - Google 및 SearX/SearXNG. 개발자는 더 많은 검색 엔진을 자유롭게 추가할 수 있습니다.

Google 검색에서 Gugal을 사용하려면 CSE ID와 API 키가 필요합니다. 둘 다 얻기 위한 지침은 앱에서 찾을 수 있습니다. <b>이를 얻으려면 Google 계정이 있어야 하므로 Google은 여전히 검색을 귀하의 계정에 연결할 수 있습니다. 개인정보를 더 존중하는 검색엔진을 사용하고 싶다면, SearX/SearXNG 인스턴스와 함께 Gugal을 사용하세요.</b>

<i>Google 및 Google 검색은 Google LLC의 상표입니다.</i>
