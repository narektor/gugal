Gugal adalah aplikasi pencarian web yang bersih dan ringan, dengan antarmuka Material 3/You modern.

Gugal mendukung 2 mesin pencarian — Google dan searx/SearXNG. Para pengembang dipersilakan untuk menambahkan lebih banyak mesin pencarian.

Untuk menggunakan Gugal dengan Google Penelusuran, Anda membutuhkan ID dan kunci API CSE. Petunjuk untuk mendapatkan keduanya dapat ditemukan dalam aplikasi. <b>Untuk mendapatkannya, Anda harus memiliki akun Google, oleh karena itu Google mungkin masih mengaitkan pencarian ke akun Anda. Jika Anda ingin menggunakan mesin pencarian yang mendukung privasi Anda secara lebih, gunakan Gugal dengan sebuah instansi searx/SearXNG.</b>

<i>Google dan Google Penelusuran adalah merek dagang dari Google LLC.</i>
