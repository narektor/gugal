- Søk kan nå startes fra delingsmenyen ved å dele hvilken som helst tekst til Gugal.
- Støtte for lenker til populære søkemotorer er lagt til. Se https://gugal.gitlab.io/launchers.html for mer info.
- Støtten for nettbrett er forbedret.
- Lagt til en «Endre drakt»-innstilling for å bytte mellom system, lys og mørk drakt.
- SearX: tillagt knapp for å sjekke om en instans kan brukes med Gugal.
- Indonesisk oversettelse ble lagt til.
- Mindre feilrettinger og forbedringer.
