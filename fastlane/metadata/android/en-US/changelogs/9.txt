- Added support for searching using searx or SearXNG. Please note that only instances with API access are supported. Most public instances don't have API access enabled, and it's recommended to self host searx or SearXNG for that reason.
- When Gugal is launched for the first time after an update a list of the changes in that update is shown.
- Added an about page, replacing multiple settings items.
- Added an option to change the credentials.
- Added Russian translation.
- Added Norwegian Bokmål translation (@kingu).
- Fixed scrolling in settings and results.
- Fixed the version text in the Settings page being black.
- On Android 11 and earlier Gugal now uses a light blue color scheme.

For developers:
- Support for more web search engines and on-device searching can be added with SERP providers. To start developing SERP providers see gugal.gitlab.io/serp.
