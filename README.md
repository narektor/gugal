# Gugal <img src="fastlane/metadata/android/en-US/images/icon.png" width="30px" alt="Icon"/>

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/com.porg.gugal/) [<img src="https://gitlab.com/narektor/gitlab-badges/-/raw/main/language/en/en-available-on.png" alt="Available on GitLab" height="80">](https://gitlab.com/narektor/gugal/-/releases)

A clean, lightweight, FOSS web search app.

<div width="100%" height="auto">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="200px" alt="Main screenshot"/> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="200px" alt="Setup wizard"/> <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="200px" alt="News"/>
</div>

## Download

Gugal can be downloaded from [the GitLab releases page](https://gitlab.com/narektor/gugal/-/releases), and is also available on F-Droid [here](https://f-droid.org/packages/com.porg.gugal/).

Download from one _or_ the other. The versions are incompatible as F-Droid signs apps themselves.

### 0.2 and 0.3

0.2 and 0.3 were removed to prevent F-Droid from releasing them as updates to newer versions. The APKs and source code can be found [here](https://gitlab.com/gugal/releases/-/tree/main/deleted-tags).

## Set up

To use Gugal with Google Search you need a CSE (Programmable Search Engine) ID and API key. Instructions for obtaining both can be found in the app.

**To get these values you must have a Google account, therefore Google might still link searches to your account.**

To use Gugal with searx or SearXNG you need an instance **with the JSON search format enabled**. We recommend making your own instance as most public instances don't have it enabled.

## Upcoming features 

### App
- [x] News tab powered by RSS
- [x] Tablet optimized UI
- [ ] Image search
- [ ] Backup and restore
- [ ] Support for multiple search pages
- [ ] Potentially an "incognito" search mode (limited by search engine support)
- [ ] Potentially Stract support (#33)

## Contributing

Please read the [contributing guidelines](CONTRIBUTING.md) too.

### Translations
If you want to translate Gugal into your language or clean up an existing translation, check out our [Weblate](https://hosted.weblate.org/engage/gugal).

### Support for search engines
To search the web Gugal uses SERP providers. You can find out more [on our site](https://gugal.gitlab.io/serp).

### Code
Gugal is built with Jetpack Compose and Kotlin.
